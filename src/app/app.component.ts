import {Component, Input} from '@angular/core';
import {CardDeck} from "../lib/CardDeck";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'blackjack';
  score = 0;
  winOrLose = '';
  cardDeck = new CardDeck();
  @Input() cards = this.cardDeck.getCards(2);

  give() {
    this.cards.push(this.cardDeck.getCard());
  }

  reset() {
    this.cardDeck = new CardDeck();
    this.cards = this.cardDeck.getCards(2);
  }

  getScore() {
    let counter = 0;
    this.cards.forEach(card => {
      counter = counter + card.getScore();
    });
    this.score = counter;
    return counter;
  }

  info() {
    if (this.score === 21) {
      return 'You win!'
    } else if (this.score > 21) {
      return 'You lose!'
    } else {
      return;
    }
  }

  disBtn() {
    return this.score >= 21;
  }
}



import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent {
  @Input() rank = 'K';
  @Input() suit = 'diams';

  getClassname() {
    return `card rank-${this.rank.toLocaleLowerCase()} ${this.suit}`;
  }

  getSuitName() {
    if (this.suit === "diams") {
      return '♦';
    } else if (this.suit === "hearts") {
      return '♥';
    } else if (this.suit === "clubs") {
      return '♣';
    } else if (this.suit === "spades") {
      return '♠';
    } else {
      return;
    }
  }
}

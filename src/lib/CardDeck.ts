const RANKS = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
const SUITS = ['diams', 'hearts', 'clubs', 'spades'];

export class Card {
  rank: string;
  suit: string;
  constructor(rank:string, suit:string) {
    this.rank = rank;
    this.suit = suit;
  }
  getScore() {
    if (this.rank === "2" || this.rank === "3" || this.rank === "5" || this.rank === "6" || this.rank === "7" || this.rank === "8" || this.rank === "9" || this.rank === "10") {
      return parseInt(this.rank);
    } else if (this.rank === "J" || this.rank === "Q" || this.rank === "K") {
      return 10;
    } else if (this.rank === "A") {
      return 11;
    } else {
      return 0;
    }
  }
}

export class CardDeck {
  cards: Card[] = [];
  constructor() {
    for (let i = 0; i < RANKS.length; i++) {
      for (let j = 0; j < SUITS.length; j++) {
        const card = new Card(RANKS[i], SUITS[j]);
        this.cards.push(card);
      }
    }
  }
  getCard(): Card {
    const random = Math.floor(Math.random() * this.cards.length);
    const removed = this.cards.splice(random, 1);
    return removed[0];
  }

  getCards(howMany: number): Card[] {
    const arr: Card[] = [];
    for (let i = 0; i < howMany; i++) {
      arr.push(this.getCard())
    }
    return arr;
  }
}
